real.raw <- read.csv('kc_house_data.csv')

str(real.raw)
summary(real.raw)


real <- real.raw

library(ggplot2)

#price dustribution 
ggplot(real, aes(price)) + geom_histogram(binwidth = 50000)

#extract year from date (only year is meaningful for real estate prices)
date.ch <- as.character(real$date)
year <- substr(date.ch,1,4)
real$year <- year
real$year <- as.factor(real$year)

#How year affects prices
ggplot(real, aes(as.factor(year),price)) + geom_boxplot()


#how bedrooms affects prices 
ggplot(real, aes(bedrooms)) + geom_histogram(binwidth = 1)
ggplot(real, aes(bedrooms,price)) + geom_abline()

#how sqft_living affects prices 
ggplot(real, aes(sqft_living)) + geom_histogram()
ggplot(real, aes(as.factor(bedrooms),price)) + geom_boxplot()

#how view affects prices 
ggplot(real, aes(as.factor(view),price)) + geom_boxplot()
#since the graph shows a relation between the view and the 
#price we will leave it as an integer 

#Anlize zipcode distribution (can we extract somethng from it?)
ggplot(real, aes(zipcode)) + geom_histogram(binwidth = 1)
zip <- as.character(real$zipcode)
zip1  <- substr(zip,3,4)
real$zip1 <- zip1
real$zip1 <- as.factor(real$zip1)
ggplot(real, aes(as.factor(zip1),price)) + geom_boxplot()

#how lat affects prices 
ggplot(real, aes(lat,price)) + geom_point() + geom_smooth(method = 'lm')
#(Looks like that going north makes houses more costly)

#how long affects prices 
ggplot(real, aes(long,price)) + geom_point() + geom_smooth(method = 'lm')
#longitude does not affect pricesso we will drop it

#how year built affects prices 
ggplot(real, aes(yr_built,price)) + geom_point() + geom_smooth(method = 'lm')
#year built does not seen to affect prices 


#preparing for modeling 
#removing unnecessary attributes 

real$id <- NULL
real$date <- NULL 
real$zipcode <- NULL
real$lat <- NULL 
real$yr_built <- NULL 



#divide into trainig set and test set 
library(caTools)

filter <- sample.split(real$price , SplitRatio = 0.7)

real.train <- subset(real, filter ==T)
real.test <- subset(real, filter ==F)


#applying the linear regression model 
model <- lm(price ~ ., real.train)

summary(model)
#looks like most features are meaningfull 
predicted.train <- predict(model,real.train)
predicted.test <- predict(model,real.test)


MSE.train <- mean((real.train$price - predicted.train)**2)
MSE.test <- mean((real.test$price - predicted.test)**2)

RMSE.train <- MSE.train**0.5 #209655
RMSE.test <- MSE.test**0.5 #211315
#there is a very slight overfitting 


#what is the mean price
mprice <- mean(real$price) # 540088

#error in percantage of the mean
per_error <-RMSE.test/mprice #40% prety large error 


























